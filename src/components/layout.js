import * as React from 'react'
import { Link } from 'gatsby'
import { Navbar } from './Navbar'

const Layout = ({ pageTitle, children }) => {
  return (
    <div className='container'>
      <header className='flex justify-around w-screen'>
      <h1 className="w w-screen  text-center text-3xl p-10 bg-amber-400 text-white uppercase">{pageTitle}</h1>
      <Navbar className=" align-bottom"/>
      </header>
      <main className='w-screen  h-96 mx-auto flex justify-center align-bottom flex-col text-center bg-slate-100'>
        
        {children}
      </main>
    </div>
  )
}
export default Layout