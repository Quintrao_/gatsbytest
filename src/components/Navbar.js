import { Link } from 'gatsby'
import * as React from 'react'

export const Navbar = () => {
    return (
        <nav className=" bg-amber-400 w-screen flex justify-center align-middle">
            <ul className="flex gap-10 align-top justify-around p-5 uppercase text-white font-bold text-lg">
            <li className="hover:scale-125 hover:underline hover:opacity-75 transition-all duration-300"><Link to="/"> Home </Link></li>
            <li className="hover:scale-125 hover:underline hover:opacity-75 transition-all duration-300"><Link to='/about'> About</Link></li>
            <li className="hover:scale-125 hover:underline hover:opacity-75 transition-all duration-300"><Link to='/items/first'> First</Link></li>
            <li className="hover:scale-125 hover:underline hover:opacity-75 transition-all duration-300"><Link to='/items/second'> Second</Link></li>
            </ul>
        </nav>
    )
}