import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout pageTitle="Home Page" >
    <Seo title="Home" />
    <div className="flex flex-col justify-center align-middle">
    <h1 className="uppercase">So</h1>
    <p>First try to make something on Gatsby</p>
    </div>
  </Layout>
)

export default IndexPage
