import * as React from 'react'
import Layout from '../../components/layout'

const First = () => {
    return (
        <Layout pageTitle='SomePage'>
        <h3>this is first item</h3>
        <p> looks like it works</p>
        </Layout>
    )

}

export default First