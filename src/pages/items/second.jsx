import { StaticImage } from 'gatsby-plugin-image'
import * as React from 'react'
import Layout from '../../components/layout'

const Second = () => {
    return (
        <Layout pageTitle="Page with IMG">
        <h3>this is second item</h3>
        <p> looks like it works too. And a picture!</p>
        <StaticImage
            src='https://i.pinimg.com/originals/4b/a9/c6/4ba9c61d35a0057c520a68e5cfb0774b.jpg'
            alt='Собакен'
            className=" max-w-200px h-screen"
        />

        
        </Layout>
    )

}

export default Second